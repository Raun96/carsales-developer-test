# About the Project

	This application allows users to browse a list of cars that are currently available for sale at Carsales, and know the details about them.
	
	The application was developed using Xcode 10.2.1, with the deployment target being 12.2 
	It supports universal devices i.e all devices currently supported by iOS 12.

# Third Party Libararies
	
	There were no 3rd party libraries that were used in development of this project.
	
# Architecture

	The app follows the MVC architecture.
	
	Model contains the Car.swift and CarDetails.swift
	View contains the CarsListingCell.swift and CarDetailsImageCell.swift
	Controller contains the MainVC.swift and DetailsVC.swift
	
	NetworkingServices contains ListingNetworkingService.swift and CarDetailsNetworkingService.swift
	Both classes help in making network calls to the CarSales API for getting the relevant information back in JSON. 
	
	Main.storyboard contains the scene's for the ViewControllers.
	
# Approach

	- Used Postman to hit the different endpoints provided to get a better understanding of how the GET Request and the returned JSON is structured.
	
	- Modeled the Structs to match the JSON response using the Codable protocol, so that its esaier to decode the JSON.
	
	- ViewConrollers Structure.
		- This app has two View Controllers MainVC and Details VC.
		- Both the VC's are embedded in a Navigation Controller, so that its easier to transition between the MainVC and DetailsVC.
		- MainVC contains a tableView that helps in displaying the different cars.
		- DetailsVC contains a collectionView that shows the different images scrollable from left to right, it also contains labels and textView that displays the relevant information about the car's details.
		- both VC's are designed and layed out using autolayout in storyboard
		
	- Networking Services has two helper classes that makes a Get request to the CarSales Api and gets the relevant JSON response, then using the JSONDecoder and Decodable protocol the JSON is decoded and stored
	  the relevant structs and passed back to the consumer VC using completion handler.
	- Both classes also download the images of cars using different methods and Cache it using NSCache for a better User Experience and not making a network call to download them everytime.
	
	
# Bonus- Detail Screen

	In the Detail screen shows multiple photos of Cars that are scrollable.
	Achieved this using CollectionView and downloading image for each cell in the collectionView and then Caching them.
	
# Time Taken to complete the project

	The total time taken to complete this test was approximately 6 hours.
	
	Understanding the structure of GET request and the returned JSON - ~ 20-30 mins
	Understanding the Requirements and planning the app - ~ 30 mins
	Modelling the returned JSON into structs using Codable - ~ 30 mins
	Laying out views using autolayout and storyboard - ~ 30-45 mins
	Making a network call using URLSession to download list of cars and photo - ~ 30 mins
	Making a network call using URLSession to download details of a car - ~ 30 mins
	Setting up tableView and dequeing cells to display data in MainVC - ~ 30 mins
	Segueing to the DetailsVC and passing data - ~ 15 mins
	Setting up CollectionView and other Views to display data in DetailsVC - ~ 45 mins
	Refining CollectionView to scroll properly - ~ 15 mins
	Implementing Cacheing to reduce network calls and better UX - ~ 20-30 mins
	
	Additional time make sure everything is perfect - ~30 mins
	
	
# What's Next

	Currently learning about Unit testing, so that i can write and test them against my code.
	
		