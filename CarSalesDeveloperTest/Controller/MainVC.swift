//
//  ViewController.swift
//  CarSalesDeveloperTest
//
//  Created by Raunak Singh on 11/7/19.
//  Copyright © 2019 Raunak Singh. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    @IBOutlet var carListTableView: UITableView!
    
    var cars : [Car] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        downloadCarsList()
        configuretableView()
        self.navigationController?.navigationBar.topItem?.title = "Car List"
        
    }
    
    func configuretableView(){
        
        carListTableView.delegate = self
        carListTableView.dataSource = self
        carListTableView.rowHeight = UITableView.automaticDimension
        carListTableView.estimatedRowHeight = 400
        
    }
    
    func downloadCarsList() {
        
        ListingNetworkingService.shared.downloadCarsList { (status,cars) in
            if status {
                self.cars = cars
                self.carListTableView.reloadData()
            }
                else {
                    let alert = UIAlertController(title: "Can't Load Data", message: "Unforntunately listing data can't be loaded at this time. Try Again Later.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel
                        , handler: { _ in
                            print("An error occured while downloading car List. Response Code = \(status)")
                    }))
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                }
        }
        
    }

}

extension MainVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = carListTableView.dequeueReusableCell(withIdentifier: "CarsListingCell") as? CarsListingCell {
            cell.updateCell(car: cars[indexPath.row])
            cell.selectionStyle = .none
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetailsVC", sender: carListTableView.cellForRow(at: indexPath))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailsVC" {
            if let selectedCell = sender as? CarsListingCell {
                let destinationVC = segue.destination as? DetailsVC
                let indexpath = self.carListTableView.indexPath(for: selectedCell)
                destinationVC?.detailsURL = self.cars[(indexpath?.row)!].DetailsUrl
            }
        }
    }
    
}

