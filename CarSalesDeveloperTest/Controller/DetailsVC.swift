//
//  DetailsVC.swift
//  CarSalesDeveloperTest
//
//  Created by Raunak Singh on 11/7/19.
//  Copyright © 2019 Raunak Singh. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {
    
    @IBOutlet var carImagesCollectionView: UICollectionView!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var saleStatusLbl: UILabel!
    @IBOutlet var commentsTextView: UITextView!
    @IBOutlet var loader: UIActivityIndicatorView!
    
    var detailsURL = String()
    var carImages = [String]()
    let detailsNetworkingService = CarDetailsNetworkingService()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loader.startAnimating()
        downloadCarDetails()
        setUpCarImagesCollectionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        commentsTextView.isScrollEnabled = true
    }
    
    func setUpCarImagesCollectionView(){
        carImagesCollectionView.delegate = self
        carImagesCollectionView.dataSource = self
        carImagesCollectionView.allowsSelection = false
        carImagesCollectionView.isPagingEnabled = true
    }
    
    
    func downloadCarDetails() {
        
        detailsNetworkingService.downloadCarDetails(detailsURL: detailsURL) { (status, details) in
            
            if status {
                self.carImages = details[0].Overview.Photos
                self.carImagesCollectionView.reloadData()
                self.locationLbl.text = details[0].Overview.Location
                self.priceLbl.text = details[0].Overview.Price
                self.saleStatusLbl.text = details[0].SaleStatus
                self.commentsTextView.text = details[0].Comments
                self.loader.stopAnimating()
            }
                else {
                    let alert = UIAlertController(title: "Can't Load Data", message: "Unforntunately car details can't be loaded at this time. Try Again Later.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel
                        , handler: { _ in
                            self.navigationController?.popViewController(animated: true)
                    }))
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                }
        }
        
    }
   
}

extension DetailsVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return carImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = carImagesCollectionView.dequeueReusableCell(withReuseIdentifier: "CarDetailsImageCell", for: indexPath) as? CarDetailsImageCell {
            cell.updateCell(imageURL: carImages[indexPath.row])
            cell.sizeThatFits(CGSize(width: self.carImagesCollectionView.frame.width, height: self.carImagesCollectionView.frame.height))
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
}

extension DetailsVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = carImagesCollectionView.frame.size
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
