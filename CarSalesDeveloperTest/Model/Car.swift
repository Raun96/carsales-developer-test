//
//  Car.swift
//  CarSalesDeveloperTest
//
//  Created by Raunak Singh on 11/7/19.
//  Copyright © 2019 Raunak Singh. All rights reserved.
//

import Foundation

struct Car: Codable {
    
    var Id : String
    var Title : String
    var Location : String
    var Price : String
    var MainPhoto : String
    var DetailsUrl : String
    
}

struct carsList : Codable {
    
    var Result : [Car]
    
}
