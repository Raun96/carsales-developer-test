//
//  CarDetails.swift
//  CarSalesDeveloperTest
//
//  Created by Raunak Singh on 11/7/19.
//  Copyright © 2019 Raunak Singh. All rights reserved.
//

import Foundation

struct CarDetails : Codable {
    
    var Id : String
    var SaleStatus : String
    var Comments: String
    var Overview : VehicleOverview
    
}

struct VehicleOverview : Codable {
    
    var Location : String
    var Price : String
    var Photos : [String]
    
}

struct CarDetailsResult : Codable {
    
    var Result : [CarDetails]
    
}
