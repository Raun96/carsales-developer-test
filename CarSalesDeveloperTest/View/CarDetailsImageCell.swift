//
//  CarDetailsImageCell.swift
//  CarSalesDeveloperTest
//
//  Created by Raunak Singh on 12/7/19.
//  Copyright © 2019 Raunak Singh. All rights reserved.
//

import UIKit

class CarDetailsImageCell: UICollectionViewCell {
    
    let detailsNetworkingService = CarDetailsNetworkingService()
    
    @IBOutlet var carImage: UIImageView!
    
    func updateCell(imageURL: String) {
        detailsNetworkingService.searchCarPhoto(imageUrl: imageURL) { (image) in
            self.carImage.image = image
        }
    }
}
