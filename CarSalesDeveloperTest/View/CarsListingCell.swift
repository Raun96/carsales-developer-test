//
//  CarsListingCell.swift
//  CarSalesDeveloperTest
//
//  Created by Raunak Singh on 11/7/19.
//  Copyright © 2019 Raunak Singh. All rights reserved.
//

import Foundation
import UIKit

class CarsListingCell: UITableViewCell {
    
    @IBOutlet var carImage: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var location: UILabel!
    
    func updateCell(car : Car) {
        
        title.text = car.Title
        price.text = car.Price
        location.text = car.Location
        ListingNetworkingService.shared.searchMainCarPhoto(car: car) { (carImage) in
            self.carImage.image = carImage
        }
        
    }
    
}
