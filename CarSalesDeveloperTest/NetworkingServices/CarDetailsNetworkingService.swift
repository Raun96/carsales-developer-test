//
//  CarDetailsNetworkingService.swift
//  CarSalesDeveloperTest
//
//  Created by Raunak Singh on 11/7/19.
//  Copyright © 2019 Raunak Singh. All rights reserved.
//

import Foundation
import UIKit

class CarDetailsNetworkingService {
    
    static let shared = CarDetailsNetworkingService()
    
    // This is the cache to store the images, so that they don't have to be downloaded everytime.
    var imageCache = NSCache<NSString,UIImage>()
    
    private let baseCarsalesURL = "https://app-car.carsalesnetwork.com.au"
    private let apiParameters = ["username" : "test", "password" : "2h7H53eXsQupXvkz"]
    
    
    /// This method makes a get request to the Carsales Car Details API.
    ///
    /// - Parameters:
    ///   - detailsURL: A string containing details URl for a specific car.
    ///   - downloadComplete: a completion handler that gets called everytime the session has finished downloading the details of a car. It returns the status(if the response from the server is OK or not) and an array of CarDetails.
    func downloadCarDetails(detailsURL: String, downloadComplete : @escaping (_ status : Bool, _ details : [CarDetails]) -> ()) {
        
        var urlParameters = [URLQueryItem]()
        var carSalesDetailsURLComponents = URLComponents(string: baseCarsalesURL + detailsURL)
        var downloadStatus = false
        var carDetails : [CarDetails] = []
        
        for parameter in apiParameters {
            urlParameters.append(URLQueryItem(name: parameter.key, value: parameter.value))
        }
        
        carSalesDetailsURLComponents!.queryItems = urlParameters
        
        guard let carSalesDetailsURL = carSalesDetailsURLComponents!.url else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: carSalesDetailsURL) { (data, response, error) in
            
            let status = (response as! HTTPURLResponse).statusCode
            
            // Check the response status i.e. 200 means OK.
            if status == 200 {
                guard let data = data else {
                    //handle the error
                    print("No data found!!!")
                    downloadComplete(downloadStatus,carDetails)
                    return
                }

                let decoder = JSONDecoder()
                do {
                    let details = try decoder.decode(CarDetailsResult.self, from: data)
                    carDetails = details.Result
                    downloadStatus = true
                } catch {
                    print("error!!!")
                }
            }
            
            DispatchQueue.main.async {
                downloadComplete(downloadStatus,carDetails)
            }
            
        }
        task.resume()
    }
    
    
    /// This function checks for the main image of the car in the imageCache by the imageURL, if not found calls the downloadCarMainPhoto() form the ListingNetworkingServices().
    ///
    /// - Parameters:
    ///   - imageUrl: A string containing image URl for a specific car.
    ///   - imageRetrived: a completion handler that gets called when the image is retrived from the imageCache or when it has been downloaded and returned by downloadCarMainPhoto(). It returns a UIImage.
    func searchCarPhoto(imageUrl: String, imageRetrived : @escaping (_ carImage : UIImage) ->()) {
        if let imageFromCache = imageCache.object(forKey: imageUrl as NSString) {
            imageRetrived(imageFromCache)
        } else {
            ListingNetworkingService.shared.downloadCarMainPhoto(imageUrl: imageUrl) { (carImage) in
                self.imageCache.setObject(carImage, forKey: imageUrl as NSString)
                imageRetrived(carImage)
            }
        }
        
    }
}
