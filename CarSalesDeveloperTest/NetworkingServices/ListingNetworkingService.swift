//
//  networking.swift
//  CarSalesDeveloperTest
//
//  Created by Raunak Singh on 11/7/19.
//  Copyright © 2019 Raunak Singh. All rights reserved.
//

import Foundation
import UIKit

class ListingNetworkingService {
    
    static let shared = ListingNetworkingService()
    
    // This is the cache to store the images, so that they don't have to be downloaded everytime.
    let imageCache = NSCache<NSString, UIImage>()
    
    private let baseCarsalesURL = "https://app-car.carsalesnetwork.com.au"
    private let listingURL = "/stock/car/test/v1/listing"
    private let apiParameters = ["username" : "test", "password" : "2h7H53eXsQupXvkz"]
    
    /// This method makes a get request to the Carsales listing API.
    ///
    /// - Parameter downloadComplete: a completion handler that gets called everytime the session has finished downloading the list of cars. It returns the status(if the response from the server is OK or not) and an array of Cars.
    func downloadCarsList(downloadComplete : @escaping (_ status : Bool, _ cars : [Car]) -> ()) {
        
        var urlParameters = [URLQueryItem]()
        var cars : [Car] = []
        var downloadStatus = false
        
         var carSalesListingURLComponent = URLComponents(string: baseCarsalesURL + listingURL)
        
        for parameter in apiParameters {
            urlParameters.append(URLQueryItem(name: parameter.key, value: parameter.value))
        }
        
        carSalesListingURLComponent!.queryItems = urlParameters
        
        guard let carSalesListingURL = carSalesListingURLComponent!.url else {
            downloadComplete(false, cars)
            return
        }
        
        let task = URLSession.shared.dataTask(with: carSalesListingURL) { (data, response, error) in
            
            let status = (response as! HTTPURLResponse).statusCode
            
            // Check the response status i.e. 200 means OK.
            if status == 200 {
                
                guard let data = data else {
                    //handle the error
                    print("No data found!!!")
                    downloadComplete(downloadStatus, cars)
                    return
                }
                
                let decoder = JSONDecoder()
                do {
                    let carList = try decoder.decode(carsList.self, from: data)
                    downloadStatus = true
                    cars = carList.Result
                } catch {
                    print("error!!!")
                }
                
            }
            
            DispatchQueue.main.async {
                downloadComplete(downloadStatus,cars)
            }
            
        }
        task.resume()
      
    }
    
    /// This function checks for the main image of the car in the imageCache by the car's ID, if not found calls the downloadCarMainPhoto().
    ///
    /// - Parameters:
    ///   - car: A car object that holds the details of the car.
    ///   - imageRetrived: a completion handler that gets called when the image is retrived from the imageCache or when it has been downloaded and returned by downloadCarMainPhoto(). It returns a UIImage.
    func searchMainCarPhoto(car : Car, imageRetrived : @escaping (_ carImage : UIImage) ->()) {
        if let imageFromCache = imageCache.object(forKey: car.Id as NSString) {
            imageRetrived(imageFromCache)
        } else {
            downloadCarMainPhoto(imageUrl: car.MainPhoto) { (carImage) in
                self.imageCache.setObject(carImage, forKey: car.Id as NSString)
                imageRetrived(carImage)
            }
        }
        
    }
    
    /// This functions downloades the mainImage for a car Listing, if its not present in the imageCache.
    ///
    /// - Parameters:
    ///   - imageUrl: A string that holds the URL for the image
    ///   - downloadComplete: a completion handler that gets called everytime the session has finished downloading the car image. It returns a UIImage.
    func downloadCarMainPhoto(imageUrl: String, downloadComplete : @escaping (_ carImage: UIImage)->()) {
        
        guard let imageURL = URL(string: imageUrl) else {
            //handle error for invalid URL.
            print("error in the URL.")
            return
        }
        
        let task = URLSession.shared.dataTask(with: imageURL) { (data, response, error) in
            let status = (response as! HTTPURLResponse).statusCode
            
            // Check the response status i.e. 200 means OK.
            if status == 200 {
                
                guard let imagedata = data else {
                    return
                }
                
                guard let image = UIImage(data: imagedata) else {
                    return
                }
                
                DispatchQueue.main.async {
                    downloadComplete(image)
                }
                
            } else {
                //Handle the response error.
                //pass on some demo stock Image.
                print("Unfortunately image Can't be loaded.")
            }

        }
        task.resume()
    }
}
